import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const XylophoneApp());
}

class XylophoneApp extends StatelessWidget {
  const XylophoneApp({Key? key}) : super(key: key);

  void playSound(int soundNumber) {
    final player = AudioCache();
    player.play('note$soundNumber.wav');
  }

  Expanded buildXylophoneKey({required int soundNumber}) {
    final colors = List<Color>.from([
      Colors.red,
      Colors.blue,
      Colors.green,
      Colors.yellow,
      Colors.pinkAccent,
      Colors.deepPurple,
      Colors.orangeAccent
    ], growable: false);

    return Expanded(
      child: TextButton(
        onPressed: () {
          playSound(soundNumber);
        },
        child: Container(color: colors[soundNumber - 1]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.deepOrange,
      home: Scaffold(
        backgroundColor: Colors.brown.shade500,
        body: SafeArea(
          child: Center(
            child: Column(
              children: <Widget>[
                buildXylophoneKey(soundNumber: 1),
                buildXylophoneKey(soundNumber: 2),
                buildXylophoneKey(soundNumber: 3),
                buildXylophoneKey(soundNumber: 4),
                buildXylophoneKey(soundNumber: 5),
                buildXylophoneKey(soundNumber: 6),
                buildXylophoneKey(soundNumber: 7),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
